// window.onload = function() {
function show_slices() {
    //
    // create the 2D renderers
    // .. for the X orientation
    var sliceX = new X.renderer2D();
    sliceX.container = 'sliceX';
    sliceX.orientation = 'X';
    sliceX.init();
    // .. for Y
    var sliceY = new X.renderer2D();
    sliceY.container = 'sliceY';
    sliceY.orientation = 'Y';
    sliceY.init();
    // .. and for Z
    var sliceZ = new X.renderer2D();
    sliceZ.container = 'sliceZ';
    sliceZ.orientation = 'Z';
    sliceZ.init();


    //
    // THE VOLUME DATA
    //
    // create a X.volume
    volume = new X.volume();
    // .. and attach the single-file dicom in .NRRD format
    // this works with gzip/gz/raw encoded NRRD files but XTK also supports other
    // formats like MGH/MGZ
    volume.file = 'https://dl.dropboxusercontent.com/s/6ys46h46xlbbeyw/test.vtk';
    // https://www.dropbox.com/s/6ys46h46xlbbeyw/test.vtr?dl=0
    // // we also attach a label map to show segmentations on a slice-by-slice base
    // volume.labelmap.file = 'http://x.babymri.org/?seg.nrrd';
    // // .. and use a color table to map the label map values to colors
    // volume.labelmap.colortable.file = 'http://x.babymri.org/?genericanatomy.txt';

    // add the volume in the main renderer
    // we choose the sliceX here, since this should work also on
    // non-webGL-friendly devices like Safari on iOS
    sliceX.add(volume);

    // start the loading/rendering
    sliceX.render();


    //
    // THE GUI
    //
    // the onShowtime method gets executed after all files were fully loaded and
    // just before the first rendering attempt
    sliceX.onShowtime = function () {

        //
        // add the volume to the other 3 renderers
        //
        sliceY.add(volume);
        sliceY.render();
        sliceZ.add(volume);
        sliceZ.render();

        // now the real GUI
        var gui = new dat.GUI();

        // the following configures the gui for interacting with the X.volume
        var volumegui = gui.addFolder('Volume');
        var lowerThresholdController = volumegui.add(volume, 'lowerThreshold',
            volume.min, volume.max);
        var upperThresholdController = volumegui.add(volume, 'upperThreshold',
            volume.min, volume.max);

        // the indexX,Y,Z are the currently displayed slice indices in the range
        // 0..dimensions-1
        var sliceXController = volumegui.add(volume, 'indexX', 0,
            volume.dimensions[0] - 1);
        var sliceYController = volumegui.add(volume, 'indexY', 0,
            volume.dimensions[1] - 1);
        var sliceZController = volumegui.add(volume, 'indexZ', 0,
            volume.dimensions[2] - 1);
        volumegui.open();

    };

};
