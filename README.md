# prospr-dash

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7386336.svg)](https://doi.org/10.5281/zenodo.7386336)

A simple `dash` app to browse averaged _in-situ_ hybridisation data generated in the Arendt lab, at EMBL Heidelberg. These come from a protocol named [ProSPr](https://pubmed.ncbi.nlm.nih.gov/28584082/) that takes advantage of the fact that the lecithotrophic larvae of _Platynereis dumerilii_ grow more or less stereotypically in the first six days of their development, up to the late nectochaete stage. The app lets users choose any two genes that are available for each larval stage and plots their averaged expression and regions of overlap in the animal body.

## Motivation
When the ProSPr paper was published it represented a hallmark event for non-model organisms. This was a whole-body expression atlas with (up to) cellular resolution! The data was deposited online, so it was technically available for everyone to use. However, my interactions with the _Platynereis_ community and even the Arendt lab itself showed me that in practice nobody was _actually_ looking at the data. The amount of effort needed to find the gene you were interested in, locate the file, figure out if it was processed or not, open it, and scroll through the image stacks was too much for almost everybody. According to my experience, this tends to be the case with all data that is big and complex: for users to interact with it they must really, really, _really_ care about it a lot.

So, to minimize that effort for me, I built a small python notebook that let me scroll through the image stacks. It [took some time to build](https://xkcd.com/1205/) but was instrumental for the annotation of the single-cell data I was working with. Then, since I'd built it anyway, I figured it would be nice to share with the lab and the community in general. This was also a nice opportunity to learn (more) about [plotly](https://plotly.com/), [dash](https://dash.plotly.com/introduction), [docker](https://www.docker.com/), and [cloud storage](https://aws.amazon.com/s3/).

## Usage
Navigate to [under construction](https://platynereis.com/). For an overview of what is available, as well as gene names in the more obscure cases, please consider this [table](https://docs.google.com/spreadsheets/d/1v1aF9Eh3bfnWpeJ64xkHGbIzAx01jz04biYjvKGNS6g/edit?usp=sharing).

## Support
Shoot Niko an email at `nikolaos.papadopoulos[at]univie.ac.at`.

## Roadmap
I will try to upload the entire Arendt lab collection for browsing, as far as that doesn't interfere with ongoing projects:

- [ ] 16hpf
- [ ] 20hpf
- [x] 24hpf
- [ ] 28hpf
- [x] 36hpf
- [x] 48hpf
- [ ] 56hpf
- [ ] 72hpf
- [x] 6dpf

## Authors and acknowledgment
This project would not have been possible without the support of Jean-Karim Heriche. Many thanks from the entire Platy community 🙏

## License
[MIT License](https://opensource.org/licenses/MIT)

## Bibliography

- Vergara HM, Bertucci PY, Hantz P, Tosches MA, Achim K, Vopalensky P, Arendt D. Whole-organism cellular gene-expression atlas reveals conserved cell types in the ventral nerve cord of Platynereis dumerilii. Proc Natl Acad Sci U S A. 2017 Jun 6;114(23):5878-5885. doi: 10.1073/pnas.1610602114. PMID: 28584082; PMCID: PMC5468599.

